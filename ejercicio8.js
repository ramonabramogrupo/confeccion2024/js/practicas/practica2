let numero1 = prompt("Introduce el primer numero");
let numero2 = prompt("Introduce el segundo numero");
let suma = 0;

// procesamiento

// el problema es que el operador '+'
// es capaz concatenar cadenas de texto
// y sumar numeros

// suma = numero1 + numero2; // numero1='20', numero2='30', suma='2030'

// parseInt convierte un string en un numero entero
// parseInt(texto,base)
// parseFloat convierte un string en un numero decimal    
// parseFloat(texto,base)
// si el texto pasado no es un numero, el resultado es NaN

numero1 = parseInt(numero1);
numero2 = parseInt(numero2);

/*
existe un truco que es multiplicar el texto * 1
para convertirlo en un numero
*/
// numero1 = numero1 * 1;
// numero2 = numero2 * 1;

/**
 * Existe otra opcion que es utilizar Number
 */

// numero1 = Number(numero1);
// numero2 = Number(numero2);

/**
 * Esto lo podeis realizar en el propio prompt
 * 
 * numero1 = parseInt(prompt("Introduce el primer numero"));
 * numero1 = Number(prompt("Introduce el primer numero"));
 * numero1= prompt("Introduce el primer numero")*1;
 * 
 * numero2 = parseInt(prompt("Introduce el segundo numero"));
 * numero2 = Number(prompt("Introduce el segundo numero"));
 * numero2= prompt("Introduce el segundo numero")*1;
 */

//  let numero1 =prompt("Introduce el primer numero");
//  numero1=parseInt(numero1);

//  let numero1=parseInt(prompt("Introduce el primer numero"));


// calcular la suma
suma = numero1 + numero2;

// sacar el resultado con alert
alert(suma);

// sacar el resultado con document.write
document.write(suma);

// sacar el resultado con console.log
console.log(suma);

// sacar el resultado con innerHTML
document.querySelector("#salida").innerHTML = suma;