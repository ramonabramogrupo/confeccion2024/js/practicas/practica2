// variables y constantes

let numero1 = Number(prompt("Introduce el primer numero"));
let numero2 = Number(prompt("Introduce el segundo numero"));
let suma = 0;
let resta = 0;
let producto = 0;
let cociente = 0;

// procesamiento

suma = numero1 + numero2;
resta = numero1 - numero2;
producto = numero1 * numero2;
cociente = numero1 / numero2;

// salida

// yo prodria realizar la salida
// document.write();
// alert();
// console.log();

// como quiero escribir en la pagina en las zonas
// que he preparado

/*

+
-
*
/

++a => a=a+1
a++ => a=a+1
--a => a=a-1
a-- => a=a-1

a+=5 => a=a+5
a-=5 => a=a-5
a*=5 => a=a*5
a/=5 => a=a/5

*/


document.querySelector("#numero1").innerHTML += numero1;
document.querySelector("#numero2").innerHTML += numero2;
document.querySelector("#suma").innerHTML += suma;
document.querySelector("#resta").innerHTML += resta;
document.querySelector("#multiplicacion").innerHTML += producto;
document.querySelector("#division").innerHTML += cociente;

