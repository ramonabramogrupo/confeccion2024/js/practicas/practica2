
<table class="js-file-title">
<thead>
<tr>
<th>

INDICE

</th>
</tr>
</thead>
<tbody>
<tr>
<td>

Este es el índice. Navega por todo el contenido desde aquí. Aquí encontrarás el mapa de contenidos. Explora todo lo que tenemos para ofrecerte desde esta página.

</td>
</tr>
<tr>
<td>

[[_TOC_]]

</td>
</tr>
</tbody>
</table>


# Bloques generales en un programa JS

Normalmente en un programa de Js tendremos las siguientes zonas :

- Creacion de funciones definidas
- Creacion de variables y constantes
- Entradas
- Procesamiento
- Salidas

# Variables

Para crear las variables en js utilizamos let o var

```js
let nombreVariable = valor;
var nombreVariable = valor;
```

Seria recomendable utilizar let.

## Diferencias entre let y var

En JavaScript, var y let son dos formas de declarar variables, pero tienen diferencias importantes en cómo funcionan, especialmente en términos de alcance y comportamiento de elevación (hoisting). 

Aquí te explico las principales diferencias:

- Alcance (Scope):

var tiene un alcance de función, lo que significa que una variable declarada con var es accesible en toda la función en la que se declaró, o global si se declaró fuera de cualquier función.
let tiene un alcance de bloque, lo que significa que es accesible solo dentro del bloque (delimitado por llaves {}) en el que se declaró. 
Esto incluye bloques en bucles, condicionales y bloques explícitos.

- Hoisting (Elevación):
Las variables declaradas con var son elevadas al principio de la función o del script, lo que significa que pueden ser referenciadas antes de su declaración explícita. Sin embargo, hasta que no se llega a la línea de su declaración, tienen el valor undefined.
let, aunque técnicamente también es elevado, no permite que la variable sea accesible antes de su declaración. Si intentas acceder a una variable let antes de su declaración, obtendrás un error de "ReferenceError". Este comportamiento se conoce como "Temporal Dead Zone" (Zona Muerta Temporal).

- Redefinición y Reasignación:
var permite que una variable sea redeclarada dentro de su alcance. Esto puede llevar a errores si accidentalmente se redeclara una variable, especialmente en un alcance global o de función amplio.
let no permite la redeclaración de una variable dentro de su mismo alcance, ayudando a evitar algunos tipos de errores de programación.

- Uso en bucles:
Cuando var se usa en bucles, especialmente en bucles for, puede llevar a comportamientos inesperados donde las iteraciones del bucle pueden acabar usando el mismo valor de la variable debido a su alcance funcional.
let, al tener alcance de bloque, es generalmente más adecuado para usar en bucles, ya que cada iteración puede mantener su propio valor de la variable.

>Debido a estas diferencias, el uso de let es generalmente preferido en el desarrollo moderno de JavaScript sobre var, ya que >ofrece un mayor control sobre el alcance de las variables y reduce la posibilidad de errores comunes.

# Constantes 

Las constantes son identificadores que no pueden cambiar de valor a lo largo del programa.

Nosotros las vamos a utilizar principalmente para apuntar a elementos del DOM (elementos HTML de la pagina web)

```js
const nombreConstante = valor;
```

# Objetos

En JavaScript, los objetos son estructuras de datos fundamentales que agrupan propiedades y métodos. 

Los objetos pueden ser personalizados o pueden ser los que trae por defecto nuestro navegador.


En JavaScript, los objetos son estructuras de datos fundamentales que agrupan propiedades y métodos. Aquí te explico más detalladamente:

## Definicion de Objeto

Un objeto en JavaScript es una colección de propiedades, donde cada propiedad es una asociación entre un nombre (o clave) y un valor. 

## Propiedades

Las propiedades son los valores asociados con un objeto.  

Accedemos a las propiedades de un objeto mediante el uso de la notación de punto o la notación de corchetes. 

```js
document.URL; // devuelve la url de la web
document["URL"];
```

## Métodos

Los métodos son funciones que están asociadas como propiedades de un objeto, es decir pueden realizar operaciones. Estos métodos son capaces de acceder a los datos del objeto y son fundamentales para la programación orientada a objetos en JavaScript. 

```js
document.write("Hola clase"); // escribe en la web hola clase
```

Javascript puede utilizar por defecto todos los objetos del ambito donde estes programando. Por ejemplo si estamos en el navegador utilizara los objetos:

## Objetos disponibles

En un entorno web, JavaScript ofrece una variedad de objetos predefinidos que pueden ser utilizados para interactuar con el navegador y el contenido de la página web. 

Estos objetos forman parte del Modelo de Objeto del Documento (DOM) y de la API de navegadores web. 

Aquí te presento algunos de los objetos globales y APIs más comunes disponibles en JavaScript para un entorno web:

1. window
El objeto global window representa la ventana que contiene el documento del DOM. Este objeto contiene métodos y propiedades que permiten interactuar con el navegador. Por ejemplo, puedes usar window.location para obtener o cambiar la URL actual, y window.alert() para mostrar alertas.

2. document
Representa el documento HTML cargado en esa ventana y permite acceder y manipular su contenido. Por ejemplo, document.getElementById() o document.querySelectorAll() permiten seleccionar elementos HTML y manipularlos.

3. navigator
Contiene información sobre el navegador del usuario, como navigator.userAgent que ofrece detalles sobre el navegador, y métodos como navigator.geolocation que permite acceder a la geolocalización del usuario.

4. console
Proporciona acceso a la consola del navegador, donde se pueden enviar mensajes de log, como console.log(), console.warn(), y console.error() para ayudar en el desarrollo y depuración de código.

5. localStorage y sessionStorage
Permiten almacenar datos de manera local en el navegador del usuario. localStorage almacena datos sin fecha de expiración, mientras que sessionStorage almacena datos que persisten solo durante la sesión de la página.

6. JSON
Proporciona métodos para convertir entre cadenas JSON y objetos JavaScript, como JSON.stringify() y JSON.parse().

7. Math
Ofrece propiedades y métodos para realizar tareas matemáticas comunes.

8. Date
Permite manejar fechas y horas.

9. XMLHttpRequest y fetch
Permiten hacer solicitudes HTTP para interactuar con servidores. XMLHttpRequest es la manera más antigua, mientras que fetch ofrece una manera más moderna y basada en promesas.

10. Screen
Proporciona información sobre la pantalla del dispositivo, como su altura y anchura.

11. History
Permite manipular el historial de navegación del navegador.

# Objeto Window 

Para poder hacer referencia a un objeto de la ventana simplemente utilizamos el metodo o la propiedad, no es necesario hacer referencia que es de window

```js
window.alert("muestra mensaje");
alert("muestra mensaje"); // no es necesario indicar que es un metodo del window
```

# Objetos DOM

Para poder hacer refencia al objeto documento podemos utilizar directamente document

```js
document.write("Nos permite escribir en el body de la web");
```
## Elements del DOM

Podemos hacer referencia a cualquier elemento de la pagina web utilizando el objeto document y su metodo querySelector.

Podemos utilizar el metodo querySelector para acceder a un elemento del body y poder modificarle

```js
const div = document.querySelector("div");
```

Ahora tenemos un objeto de tipo div (element) y podemos utilizar sus metodos y propiedades.

Por ejemplo si queremos escribir en el div

```js
const div = document.querySelector("div");
div.innerHTML = "Hola clase";
```

# Entradas

Para poder introducir datos en JS podemos realizarlos de muchas maneras. 

De momento vamos a utilizar dos formas:

- metodo prompt
- Formularios

## Metodo prompt 

En JavaScript, prompt() es un método y no una instrucción. Se utiliza para mostrar un cuadro de diálogo con un mensaje opcional, que solicita al usuario que ingrese algún texto, y luego devuelve ese texto como una cadena. Si el usuario hace clic en "Cancelar", retorna null.

Para poderle utilizar

```js
let userInput = window.prompt("Por favor, introduce tu nombre:"); // no es necesario hacer referencia que es de window
```

Sin embargo, como prompt() es un método del objeto global window, puedes omitir la referencia a window y simplemente escribir:

```js
let userInput = prompt("Por favor, introduce tu nombre:");
```

En este ejemplo, prompt muestra un cuadro de diálogo que pide al usuario que introduzca su nombre. Después de que el usuario escribe su nombre y hace clic en "Aceptar", el texto ingresado se asigna a la variable userInput. Si el usuario hace clic en "Cancelar", userInput será null.

Este método es parte del objeto window y está disponible en los navegadores como una forma de interactuar con el usuario, pero no es parte del estándar ECMAScript, lo que significa que su comportamiento puede variar ligeramente entre diferentes navegadores. Además, no está disponible en entornos que no son de navegador, como Node.js.


## Formularios

Estos los veremos un poco mas adelante.

# Salidas

Para realizar las salidas tenemos muchas opciones en JS.

De momento vamos a utilizar:

- metodo write del objeto document
- metodo alert del objeto window
- propiedad innerHTML del objeto element 
- metodo log del objeto console

## Metodo Write del objeto document

El método write() del objeto document en JavaScript es utilizado para escribir una cadena de texto HTML directamente en un documento HTML. 

Este método es parte del objeto document, que representa el documento cargado en el navegador.

El método write() se puede utilizar para agregar contenido HTML al documento mientras se está procesando, es decir, mientras el navegador todavía está construyendo el DOM (Modelo de Objeto del Documento) basado en el HTML recibido. Un uso típico de document.write() se ve así:

```js
document.write("Nos permite escribir en el body de la web");
```

### Consideraciones y Comportamientos

Cuando se utiliza document.write() mientras la página está siendo cargada (por ejemplo, en un script que se ejecuta en el cuerpo del HTML), este método escribe directamente en el documento HTML y forma parte del flujo normal de la página.

Si se usa document.write() después de que la página ha terminado de cargar (por ejemplo, en respuesta a un evento como un clic de usuario), puede tener efectos no deseados. En este caso, cualquier llamada a document.write() borrará automáticamente todo el contenido del documento HTML y lo reemplazará por el nuevo contenido especificado en el método write(). Este comportamiento generalmente no es deseado y puede ser destructivo.

## Metodo alert de window

El método alert() es una función del objeto window en JavaScript que se utiliza para mostrar un cuadro de diálogo de alerta con un mensaje específico y un botón de "Aceptar" para cerrarlo. Este método es útil para informar a los usuarios sobre alguna información o advertencia durante la interacción con una página web.

La sintaxis básica del método alert() es bastante sencilla:

```js
window.alert("Este es un mensaje de alerta");
```

Sin embargo, como alert() es un método del objeto global window, puedes omitir la referencia a window y simplemente escribir:

```js
alert("Este es un mensaje de alerta");
```

>Cuando se llama a alert(), la ejecución del script se detiene hasta que el usuario hace clic en el botón "Aceptar". Esto >significa que el método alert() es bloqueante: ninguna otra operación o interacción en la página puede ocurrir hasta que el >cuadro de alerta sea cerrado. Este comportamiento puede ser útil para asegurar que el usuario vea y reconozca el mensaje, pero >debe usarse con moderación para no interrumpir negativamente la experiencia del usuario.

## Propiedad innerHTML del objeto element

La propiedad innerHTML del objeto Element en JavaScript es una de las herramientas más utilizadas para acceder y modificar el contenido HTML de un elemento en el Documento de Objetos del Modelo (DOM). 

Esta propiedad proporciona una forma sencilla de obtener o establecer el contenido HTML de un elemento específico.

Primero hay que poder acceder al objeto element para ello de momento vamos a utilizar el metodo querySelector del objeto document.

```js
const div = document.querySelector("#caja");
```
Ahora para poder escriibir en ese elemento 

```js
div.innerHTML = "contenido";
```

Si queremos leer el contenido del div

```js
alert(div.innerHTML);
```

El uso de innerHTML tiene implicaciones importantes de seguridad. Específicamente, es vulnerable a ataques de Cross-Site Scripting (XSS) si se utiliza para establecer contenido basado en entradas del usuario sin una adecuada sanitización. Un atacante podría inyectar script malicioso como parte de los datos que se establecen a través de innerHTML, que se ejecutaría cuando el HTML se renderice en el navegador.

### Metodo querySelector del document

El método querySelector() del objeto document en JavaScript es una herramienta poderosa que permite a los desarrolladores seleccionar el primer elemento del documento que coincide con un grupo especificado de selectores CSS. Este método forma parte de la interfaz del Document Object Model (DOM) y es ampliamente utilizado para manipular la estructura de la página web de manera eficiente.

```js
let elemento = document.querySelector(selectores);
```

> Selectores
>Una cadena de texto que contiene uno o más selectores CSS separados por comas. querySelector() retornará el primer elemento que >coincida con estos selectores.
>- selector por etiqueta : directamente el nombre de la etiqueta
>- selector por id : #id
>- selector por clase : .clase


## Metodo log del objeto console

En JavaScript, console es un objeto global que proporciona acceso a la consola del navegador. Se utiliza principalmente para enviar mensajes de depuración, errores, advertencias e información general durante el desarrollo de una aplicación web. La consola es una herramienta esencial para desarrolladores, ya que permite monitorear el comportamiento del código, verificar valores de variables, y diagnosticar problemas en tiempo de ejecución de una manera fácil y eficiente.

Métodos Comunes del Objeto console

Aquí hay una descripción de algunos de los métodos más utilizados proporcionados por el objeto console:

- console.log()
Es el método más utilizado para mostrar mensajes generales o valores de variables en la consola. Es útil para el seguimiento del flujo de un programa y para la depuración básica.

```js
console.log('Hola, mundo!');
```

- console.error()
Utilizado para enviar mensajes de error a la consola. Estos mensajes suelen destacarse en rojo para diferenciarlos de otros tipos de mensajes, facilitando la identificación de errores.

```js
console.error('Error: algo salió mal');
```

- console.warn()
Emite una advertencia en la consola, normalmente resaltada en amarillo, que es útil para señalar una situación que no es necesariamente un error, pero que podría llevar a problemas si no se atiende.
```js
console.warn('Advertencia: versión obsoleta del API');
```

- console.info()
Similar a console.log(), pero se utiliza para la salida de información informativa. En muchas consolas, este mensaje se mostrará con un icono de información.

```js
console.info('Información importante');
```

- console.debug()
Proporciona mensajes de depuración que pueden ser filtrados del resto de salidas en algunas consolas de desarrolladores.

```js
console.debug('Mensaje de depuración');
```

- console.table()
Muestra datos tabulares como una tabla, lo cual es útil para visualizar arreglos o objetos de forma más estructurada.

```js
console.table([{nombre: 'Alice', edad: 24}, {nombre: 'Bob', edad: 27}]);
```

- console.assert()
Imprime un mensaje de error en la consola si la afirmación es falsa. Si la afirmación es verdadera, no hará nada.
javascript

```js
console.assert(5 > 10, 'La condición es falsa');
```

- console.clear()
Limpia la consola.

```js
console.clear();
```

- console.count()
Registra el número de veces que se llama a console.count() con la misma etiqueta.

```js
console.count('Veces ejecutado');
```

- console.group() y console.groupEnd()
Permite agrupar un conjunto de mensajes de la consola juntos, lo que puede ser útil para organizar la salida relacionada en subgrupos.

```js
console.group('Grupo de Mensajes');
console.log('Mensaje 1');
console.log('Mensaje 2');
console.groupEnd();
```

>El objeto console es fundamental para la depuración y el testing durante el desarrollo web. Ayuda a entender el flujo del código, >verifica estados de aplicaciones, y permite a los desarrolladores ver y manejar los comportamientos de sus programas de manera >interactiva. Además, usar console efectivamente puede mejorar la calidad y la eficiencia del proceso de desarrollo.






